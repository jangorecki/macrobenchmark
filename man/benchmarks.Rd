\name{benchmarks}
\alias{benchmarks}
\title{ Get recently registered benchmarks }
\description{
    Function to access benchmark R script cache for timings, use when calling \code{\link{benchmark}} interactively. Cache is purged before and after every batch benchmarking run by \code{\link{batch.benchmark}}. If it is not available it will load recent timings from csv.
}
\usage{
    benchmarks(purge=FALSE, \dots)
}
\arguments{
    \item{purge}{logical if TRUE it will purge cache}
    \item{\dots}{ignored, added here for better backward compatibility in case of adding new arguments}
}
\value{
    A data.frame storing benchmark timings.
}
\examples{
benchmark(1+2)
benchmarks()

# purge cache
benchmarks(purge=TRUE)
}
