.data <- new.env()
setPackageName("macrobenchmark", .data)
.data$benchmarks <- list()

# empty set of columns in log
lg.template <- function() {
    data.frame(
        timestamp = structure(numeric(0), class = c("POSIXct", "POSIXt")),
        name = character(), # inner join / outer join / join with .EACHI / subset
        expr = character(), # deparsed expr
        version = integer(), # single integer incremented version for a unique `name` field, so `name` can be modified and it can be tracked over time
        scenario = character(), # subgrouping within `name`, use `;` for arbitrary list of metadata
        times = integer(),
        rows = integer(),
        sec_1 = numeric(),
        sec_2 = numeric(),
        sec_3 = numeric(),
        min_elapsed = numeric(),
        first_qu_elapsed = numeric(),
        median_elapsed = numeric(),
        mean_elapsed = numeric(),
        third_qu_elapsed = numeric(),
        max_elapsed = numeric()
        , stringsAsFactors=FALSE
    )
}

# benchmark expressions, evaluates expr multiple times and append results into `.data$benchmarks`
benchmark <- function(expr, name, scenario=NA_character_, rows=NA_integer_, times=getOption("macrobenchmark.times", 3L), version=NA_integer_, check, ...) {
    stopifnot(length(times)==1L, is.numeric(times), !is.na(times),
              is.character(scenario) || is.list(scenario), length(scenario)>0L)
    sub.expr = substitute(expr)
    if (missing(name)) name = paste(deparse(sub.expr), collapse="")
    if (is.list(scenario)) scenario = sapply(scenario, format, scientific=FALSE)
    if (length(scenario)>1L) scenario = paste(scenario, collapse=";")
    
    nano = requireNamespace("microbenchmarkCore", quietly=TRUE) && isTRUE(getOption("macrobenchmark.nano", TRUE))
    if (nano) {
        # calculate timing overhead
        # https://github.com/olafmersmann/microbenchmarkCore/blob/master/R/system_nanotime.R
        gc(FALSE)
        overhead = stats::median(replicate(100, {
            overhead_time = microbenchmarkCore::get_nanotime()
            on.exit(cat("woops"))
            new.overhead_time = microbenchmarkCore::get_nanotime()
            on.exit()
            new.overhead_time - overhead_time
        }))
    }
    
    elapsed = rep(NA_real_, times)
    ans = NULL # to handle error and empty result
    tryCatch({
        # it will stop on first error and leave rest of iteration timings as NA
        # we assume deterministic behavior of function which is benchmarking so fail should occur in each iteration
        # TODO: when check function results false it should not be catched by tryCatch
        for (i in 1:times) {
            gc(FALSE)
            if (nano) {
                time = microbenchmarkCore::get_nanotime()
                on.exit(cat("Timing stopped at: ", (microbenchmarkCore::get_nanotime() - time) * 1e-9, " on '",name,"': ",scenario,"\n", sep=""))
                ans = eval.parent(sub.expr)
                new.time = microbenchmarkCore::get_nanotime()
                on.exit()
                elapsed[[i]] = (new.time - time - overhead) * 1e-9
            } else {
                time = proc.time()[[3L]]
                on.exit(cat("Timing stopped at: ", proc.time()[[3L]] - time, " on '",name,"': ",scenario,"\n", sep=""))
                ans = eval.parent(sub.expr)
                new.time = proc.time()[[3L]]
                on.exit()
                elapsed[[i]] = new.time - time
            }
            if (!missing(check)) {
                stopifnot(is.function(check))
                if (!check(ans)) stop(sprintf("Benchmark '%s' produces result that failed to pass `check` function. First line of `str` in %s iteration: %s", name, i, toString(utils::capture.output(utils::str(ans))[1L])))
            }
        }
    },
    warning = function(w) warning(paste("WARNING:", w[[1L]]$message), immediate.=TRUE),
    error = function(e) warning(paste("ERROR:", e$message), immediate.=TRUE))
    l = c(list(timestamp=as.integer(Sys.time()),
               name=name,
               expr=paste(deparse(sub.expr, width.cutoff=500L), collapse=""),
               version=version,
               scenario=scenario,
               times=times,
               rows=rows),
          stats::setNames(as.list(elapsed[1:3]),
                          paste("sec", 1:3, sep="_")),
          stats::setNames(as.list(summary(elapsed)),
                          paste(c("min","first_qu","median","mean","third_qu","max"), "elapsed", sep="_")))
    # append timings to list of timings, writing to csv is made by batch.benchmark
    .data$benchmarks = c(.data$benchmarks, list(l))
    invisible(ans)
}

# access timings from interactive benchmarking with `benchmark` function from cache
benchmarks <- function(purge=FALSE, ...) {
    path = "."
    out = "benchmark.csv"
    if (purge) .data$benchmarks <- list()
    if (length(.data$benchmarks)) {
        do.call("rbind", lapply(.data$benchmarks, as.data.frame, stringsAsFactors=FALSE))
    } else if (file.exists(timings.csv <- file.path(path, out))) {
        read.table(timings.csv, sep=",", header=TRUE, stringsAsFactors=FALSE)
    } else {
        lg.template()
    }
}
