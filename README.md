# macrobenchmark [![Build Status](https://gitlab.com/jangorecki/macrobenchmark/badges/master/build.svg)](https://gitlab.com/jangorecki/macrobenchmark/builds)

Benchmark arbitrary R scripts over time using git tree history.

# Installation

```r
install.packages("macrobenchmark", repos="https://jangorecki.gitlab.io/macrobenchmark")
```

For high precision timing install also optional [microbenchmarkCore](https://github.com/olafmersmann/microbenchmarkCore).
```r
install.packages("microbenchmarkCore", repos="https://olafmersmann.github.io/drat")
```

# Usage

See [macrobenchmarking](https://gitlab.com/jangorecki/macrobenchmarking) repo for full workflow examples.  
Read macrobenchmark manual.  

# Acknowledgment

- On the machine that runs benchmark you must install macrobenchmark package and all dependencies of your package, not just current dependencies but also those used in the past as they will be needed to install older version of the package.  
- If user interface of the function you are benchmarking has changed over time you need to handle that. Something like this should work:  

```r
if ("new.arg" %in% names(formals(pkg::fun)))
    pkg::fun(...)
```

